deps:
	go mod vendor -v

run:
	go run ./cmd/migrate

test:
	go test -v --cover --coverprofile c.out ./...

devenv:
	docker-compose -f ./tests/docker-compose.yml up -d -V

devenv_down:
	docker-compose -f ./tests/docker-compose.yml down
	docker-compose -f ./tests/docker-compose.yml rm

build:
	CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -a -o ./bin/godb ./cmd/godb

connect_mysql_5:
	mysql -uuser -ppassword -h127.0.0.1 -P13306
connect_mysql_8:
	mysql -uuser -ppassword -h127.0.0.1 -P13307
connect_maria_10:
	mysql -uuser -ppassword -h127.0.0.1 -P13308

connect_postgres_9:
	psql --username=user --password --host=127.0.0.1 --port=15432 --dbname=database
connect_postgres_10:
	psql --username=user --password --host=127.0.0.1 --port=15433 --dbname=database
connect_postgres_11:
	psql --username=user --password --host=127.0.0.1 --port=15434 --dbname=database
connect_postgres_12:
	psql --username=user --password --host=127.0.0.1 --port=15435 --dbname=database
