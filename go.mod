module gitlab.com/usvc/modules/go/db

go 1.13

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/lib/pq v1.2.0
	github.com/sanity-io/litter v1.2.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.5.0
	github.com/stretchr/testify v1.3.0
	gitlab.com/usvc/modules/go/migrate v0.0.0-20191110173037-99a4eb8498e4 // indirect
)
