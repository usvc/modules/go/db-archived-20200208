package config

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ConfigTests struct {
	suite.Suite
}

func TestConfig(t *testing.T) {
	suite.Run(t, &ConfigTests{})
}

func (s *ConfigTests) TestForNoDuplicates() {
	longForms := map[string]string{}
	shortForms := map[string]string{}
	for _, value := range Flag {
		s.True(len(value[FlagLong]) > 0)
		s.True(len(value[FlagShort]) == 1)
		s.True(len(longForms[value[FlagLong]]) == 0)
		longForms[value[FlagLong]] = value[FlagLong]
		s.True(len(shortForms[value[FlagShort]]) == 0)
		shortForms[value[FlagShort]] = value[FlagShort]
	}
}
