package config

import "gitlab.com/usvc/modules/go/db/pkg/migrations"

var (
	DebugMode                 = false
	Delete                    string
	DeleteMode                = false
	Driver                    string
	MigrationDirection        = DirUp
	MigrationDirectoryListing = []string{}
	MigrationSteps            = int64(-1)
	MigrationsPath            string
	OutputFormat              = "text"
	ApplyableMigrations       migrations.Migrations
	ValidMigrations           migrations.Migrations
)

type Direction string

const (
	DirUp   Direction = "up"
	DirDown Direction = "down"
)

const (
	FlagShort = 1
	FlagLong  = 0
)

var Flag map[string][]string = map[string][]string{
	"Up":       {"up", "z"},
	"Down":     {"down", "Z"},
	"Debug":    {"debug", "x"},
	"Delete":   {"delete", "X"},
	"Steps":    {"steps", "n"},
	"Driver":   {"driver", "d"},
	"Username": {"username", "u"},
	"Password": {"password", "p"},
	"Output":   {"output", "o"},
	"Host":     {"host", "H"},
	"Port":     {"port", "P"},
	"Database": {"database", "D"},
	"Verbose":  {"verbose", "v"},
	"Yes":      {"yes", "y"},
}
