package utils

import (
	"database/sql"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	. "gitlab.com/usvc/modules/go/db/lib/config"
	. "gitlab.com/usvc/modules/go/db/lib/log"
	"gitlab.com/usvc/modules/go/db/pkg/mysql"
	"gitlab.com/usvc/modules/go/db/pkg/utils"
)

func AddConfigBool(
	flag []string,
	description string,
	defaultValue bool,
	command *cobra.Command,
	config *viper.Viper,
) {
	config.SetDefault(flag[FlagLong], defaultValue)
	command.PersistentFlags().BoolP(flag[FlagLong], flag[FlagShort], defaultValue, description)
	config.BindPFlag(flag[FlagLong], command.PersistentFlags().Lookup(flag[FlagLong]))
}

func AddConfigString(
	flag []string,
	description, defaultValue string,
	command *cobra.Command,
	config *viper.Viper,
) {
	config.SetDefault(flag[FlagLong], defaultValue)
	command.PersistentFlags().StringP(flag[FlagLong], flag[FlagShort], defaultValue, description)
	config.BindPFlag(flag[FlagLong], command.PersistentFlags().Lookup(flag[FlagLong]))
}

func AddConfigInt(
	flag []string,
	description string,
	defaultValue int64,
	command *cobra.Command,
	config *viper.Viper,
) {
	config.SetDefault(flag[FlagLong], defaultValue)
	command.PersistentFlags().Int64P(flag[FlagLong], flag[FlagShort], defaultValue, description)
	config.BindPFlag(flag[FlagLong], command.PersistentFlags().Lookup(flag[FlagLong]))
}

func CreateConnection(cmd *cobra.Command) *sql.DB {
	driverType := "mysql"
	if cmd.Flag(Flag["Driver"][FlagLong]).Value.String() != "mysql" {
		driverType = cmd.Flag(Flag["Driver"][FlagLong]).Value.String()
	}
	var createConnection utils.ConnectionCreator
	var createDSN utils.DSNCreator
	switch driverType {
	case "mysql":
		fallthrough
	default:
		Logv("using 'mysql' database driver")
		createConnection = mysql.CreateConnection
		createDSN = mysql.CreateDSN
	}
	connection, err := createConnection(createDSN(
		cmd.Flag(Flag["Username"][FlagLong]).Value.String(),
		cmd.Flag(Flag["Password"][FlagLong]).Value.String(),
		cmd.Flag(Flag["Host"][FlagLong]).Value.String(),
		cmd.Flag(Flag["Port"][FlagLong]).Value.String(),
		cmd.Flag(Flag["Database"][FlagLong]).Value.String(),
	))
	if err != nil {
		Log("encountered following error while creating connection to database: '%s'", err)
		os.Exit(1)
	}
	return connection
}

func HandleExceptionsGracefully(debugMode bool, then func()) {
	if r := recover(); r != nil {
		if debugMode {
			panic(r)
		} else {
			Log("❌ FATAL ERROR: refusing to continue because of the following reason:\n```\n%s\n```\n", r)
		}
	}
	then()
}
