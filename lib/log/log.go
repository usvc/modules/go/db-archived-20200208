package log

import (
	"encoding/json"
	"fmt"
	logger "log"

	"github.com/sanity-io/litter"
)

var VerboseMode bool

type LogFormat string

const (
	LogJSON LogFormat = "json"
	LogText LogFormat = "text"
)

func Logv(format string, args ...interface{}) {
	if VerboseMode {
		logger.Printf(format, args...)
	}
}

func Log(format string, args ...interface{}) {
	logger.Printf(format, args...)
}

func LogKeyValue(format LogFormat, key string, value interface{}) {
	switch format {
	case LogText:
		fmt.Printf("> BEGIN('%s') <\n", key)
		litter.Dump(value)
		fmt.Printf("> ENDOF('%s') <\n", key)
	case LogJSON:
		fallthrough
	default:
		result, err := json.Marshal(map[string]interface{}{key: value})
		if err != nil {
			panic(err)
		}
		fmt.Println(string(result))
	}
}
