package interfaces

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type SubcommandGetter func(config *viper.Viper) *cobra.Command
type SubcommandRunner func(command *cobra.Command, args []string)
