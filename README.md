# `usvc/go-migrate`

# Supported Databases

The latest version of the following major versions of databases are considered in the testing:

1. MySQL DB 5
2. MySQL DB 8
3. MariaDB 10
4. PostgreSQL 9
5. PostgreSQL 10
6. PostgreSQL 11
7. PostgreSQL 12

# Usage

## Migration File

Migration files written for use with `godb migrate` should contain 2 parts, the forward migration, followed by a separator (`---|---`), and then the reverse migration (the rollback). An example would be:

```sql
CREATE TABLE IF NOT EXISTS `something` (
  id INT PRIMARY KEY AUTO_INCREMENT
);
---|---
DROP TABLE `something`;
```

In the example above, the `CREATE TABLE ...` symbol would be the forward migration, and the `DROP TABLE ...` symbol would be the rollback.

## Programmatic Use

## CLI Use

### Basic Use

Assuming there exists a directory at `./path/to/migrations` with several valid files as follows:

```
20191011_111112_create_some_table.sql
20191012_121113_modify_some_table.sql
20191013_131114_drop_some_table.sql
```

And assuming a MySQL server is listening on port 3306 on the local machine with a user `user` authenticated by the passphrase `password` that has create/alter/drop table access to the database `database`, you can run the migrations as follows:

```sh
godb migrate \
  -D mysql \
  -u user \
  -p password \
  -d database \
  -H 127.0.0.1 \
  -P 3306 \
  ./path/to/migrations;
# or more verbosely...
godb migrate \
  --driver mysql \
  --user user \
  --password 'password' \
  --database database \
  --host 127.0.0.1 \
  --port 3306 \
  ./path/to/migrations;
```

By default when running the upward migrations, the tool will migrate to the latest migration.

### Doing rollbacks

To roll back add the `--down` flag:

```sh
godb migrate \
  --down \
  --driver mysql \
  --user user \
  --password 'password' \
  --database database \
  --host 127.0.0.1 \
  --port 3306 \
  ./path/to/migrations;
```

By default when running with the `--down` flag, the number of steps that is rolled back is 1.

### Migrating in single steps

To rollback just a single migration, add a `1` after the `--down` or `--up` flag. If you'd like to do 2 operations, use `2`.

```sh
godb migrate \
  --down 1 \
  --driver mysql \
  --user user \
  --password 'password' \
  --database database \
  --host 127.0.0.1 \
  --port 3306 \
  ./path/to/migrations;
```


# Further Reading

## Migration Operation Flow

1. List of files are collected from a given path
2. List of migrations are collected from the database table
3. Lists from (1) and (2) are compared using the `applied` column/field

# Development Runbook

## Testing

To run tests on the entire library, you'll need to create the Docker Compose network. This can be done using:

```sh
make devenv;
```

Then to start the tests, use:

```sh
make test
```