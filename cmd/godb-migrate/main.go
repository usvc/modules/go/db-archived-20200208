package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/usvc/modules/go/db/cmd/godb-migrate/command"
)

var cmd *cobra.Command
var config *viper.Viper

func init() {
	config = viper.New()
	cmd = command.Get(config)
}

func main() {
	cmd.Execute()
}
