package command

import (
	"database/sql"
	"fmt"
	"os"
	"path"
	"strconv"

	"github.com/spf13/cobra"
	. "gitlab.com/usvc/modules/go/db/lib/config"
	. "gitlab.com/usvc/modules/go/db/lib/log"
	"gitlab.com/usvc/modules/go/db/pkg/constants"
	"gitlab.com/usvc/modules/go/db/pkg/migration"
	mySQLMigration "gitlab.com/usvc/modules/go/db/pkg/migration/mysql"
	"gitlab.com/usvc/modules/go/db/pkg/migrations"
	mySQLMigrations "gitlab.com/usvc/modules/go/db/pkg/migrations/mysql"
	"gitlab.com/usvc/modules/go/db/pkg/utils"
)

func getDirectoryListing(absoluteMigrationPath string) []string {
	directoryListings, err := utils.GetDirectoryListing(absoluteMigrationPath)
	if err != nil {
		panic(err)
	}
	return directoryListings
}

func getMigrationDirection(cmd *cobra.Command) Direction {
	direction := DirUp
	down := cmd.Flag(Flag["Down"][FlagLong]).Value.String() == "true"
	up := cmd.Flag(Flag["Up"][FlagLong]).Value.String() == "true"
	if up && down {
		direction = DirDown
	}
	return direction
}

func getMigrationSteps(cmd *cobra.Command) int64 {
	stepsAsString := (cmd.Flag(Flag["Steps"][FlagLong]).Value.String())
	steps, err := strconv.Atoi(stepsAsString)
	if err != nil {
		panic(err)
	}
	return int64(steps)
}

func getMigrationsAbsolutePath(relativePath string) string {
	cwd := getWorkingDirectory()
	return path.Join(cwd, relativePath)
}

func getMigrationsFromDirectoryListing(driver string, directoryListing []string) (migrations.Migrations, []error) {
	var errors []error
	var m migrations.Migrations = &mySQLMigrations.Migrations{}
	for _, filepath := range directoryListing {
		switch driver {
		case "mysql":
			fallthrough
		default:
			migration, err := mySQLMigration.New(filepath)
			if err != nil {
				Log("failed to load migration '%s': %v", path.Base(filepath), err)
				errors = append(errors, err)
			} else {
				Logv("loaded migration '%s' (content hash: '%s')", migration.GetFilename(), migration.GetContentHash())
				m.Push(migration)
			}
		}
	}
	return m, errors
}

func getWorkingDirectory() string {
	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	Log("current working directory: '%s'", cwd)
	return cwd
}

func getApplyableMigrations(connection *sql.DB, validMigrations migrations.Migrations) *mySQLMigrations.Migrations {
	applyableMigrations := &mySQLMigrations.Migrations{}
	validMigrations.ForEach(func(m migration.Migration, index int) {
		Logv("retrieving remote state for migration '%s'", m.GetFilename())
		if err := m.LoadFromDB(connection); err != nil {
			panic(err)
		}
		if !m.GetApplied() {
			Log("⏯ migration '%s' has not been applied", m.GetFilename())
			applyableMigrations.Push(m)
		}
	})
	return applyableMigrations
}

func handleMigrationDeletion(connection *sql.DB, migrationToDelete string, then func()) {
	deleteStmt, err := connection.Prepare(fmt.Sprintf("DELETE FROM %s WHERE filename = ?", constants.MigrationTableName))
	if err != nil {
		panic(err)
	}
	res, err := deleteStmt.Exec(migrationToDelete)
	if err != nil {
		panic(err)
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		panic(err)
	} else if rowsAffected != 1 {
		panic(fmt.Errorf("%v rows were affected in this operation", rowsAffected))
	} else {
		Log("removed migration with filename '%s'", migrationToDelete)
	}
	then()
}

func handleMigrationTableInitialization(connection *sql.DB, createIfDoesntExist ...bool) {
	shouldCreate := len(createIfDoesntExist) == 0 || !createIfDoesntExist[0]
	if !mySQLMigrations.TableExists(connection) {
		Log("table '%s' does not exist", constants.MigrationTableName)
		if shouldCreate {
			Log("creating table '%s' now...", constants.MigrationTableName)
			if err := mySQLMigrations.Initialize(connection); err != nil {
				panic(err)
			}
		}
	} else {
		Logv("table '%s' already exists, skipping creation", constants.MigrationTableName)
	}
}
