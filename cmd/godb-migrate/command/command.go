package command

import (
	"errors"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	. "gitlab.com/usvc/modules/go/db/lib/config"
	. "gitlab.com/usvc/modules/go/db/lib/log"
	"gitlab.com/usvc/modules/go/db/lib/utils"
)

const (
	Usage            = "migrate [flags] <path_to_migrations>"
	DescriptionShort = "migrate is a database migration cli tool"
	DescriptionLong  = "A database migrator tool for Go applications"
	Example          = "" +
		"  godb-migrate -dmysql -uuser -ppassword -Hlocalhost -P3306 ./path/to/migrations\n" +
		"  godb-migrate -dpostgres -uuser -ppassword -Hlocalhost -P5432 ./path/to/migrations"
)

// Run defines the logical flow of the command
func Run(cmd *cobra.Command, args []string) {
	Log("⚙️ initializing godb-migrate...")
	DebugMode = cmd.Flag(Flag["Debug"][FlagLong]).Value.String() == "true"
	if DebugMode {
		Log("debug mode: active")
	}
	defer utils.HandleExceptionsGracefully(DebugMode, func() { os.Exit(127) })
	VerboseMode = (cmd.Flag(Flag["Verbose"][FlagLong]).Value.String() == "true")
	Logv("verbose mode: active")
	Driver = cmd.Flag(Flag["Driver"][FlagLong]).Value.String()
	Log("database driver: '%s'", Driver)
	MigrationDirection = getMigrationDirection(cmd)
	Log("migration direction: '%s'", MigrationDirection)
	MigrationSteps = getMigrationSteps(cmd)
	Log("number of steps to run: %v", MigrationSteps)
	Delete = cmd.Flag(Flag["Delete"][FlagLong]).Value.String()
	DeleteMode = len(Delete) > 0
	if DeleteMode {
		Log("delete flag has been set to remove migration with filename '%s'", Delete)
	}

	// load from local migrations source
	Log("⏱ loading local migrations...")
	relativePath := args[0]
	MigrationsPath = getMigrationsAbsolutePath(relativePath)
	Log("absolute path to migrations: '%s'", MigrationsPath)
	MigrationDirectoryListing = getDirectoryListing(MigrationsPath)
	Log("observed %v files in migration directory: \n- %v", len(MigrationDirectoryListing), strings.Join(MigrationDirectoryListing, "\n- "))
	ValidMigrations, _ = getMigrationsFromDirectoryListing(Driver, MigrationDirectoryListing)
	Log("observed %v migration files: %v", ValidMigrations.Len(), ValidMigrations)

	// load from remote migrations source
	Log("⏱ loading remote migrations...")
	connection := utils.CreateConnection(cmd)
	handleMigrationTableInitialization(connection)

	// handle deletions
	if DeleteMode {
		handleMigrationDeletion(connection, Delete, func() { os.Exit(0) })
	}

	// compare local with remote
	ApplyableMigrations := getApplyableMigrations(connection, ValidMigrations)

	if MigrationDirection == DirUp {
		if ApplyableMigrations.Len() > 0 {
			if MigrationSteps == -1 {
				Log("applying all upward migrations...")
			} else {
				Log("applying %v upwards migrations...", MigrationSteps)
			}
			ApplyableMigrations.Execute(connection, MigrationSteps)
		}
	} else {
		Log("applying %v downward migrations...", MigrationSteps)
		ApplyableMigrations.Rollback(connection, MigrationSteps)
	}
	Log("✅ database migration is complete")

	os.Exit(0)
}

// Get is used by the calling command to retrieve this for use
// as a sub-command
func Get(config *viper.Viper) *cobra.Command {
	cmd := &cobra.Command{
		Use:     Usage,
		Short:   DescriptionShort,
		Long:    DescriptionLong,
		Example: Example,
		Run:     Run,
		Args:    getArgsProcessor(config),
	}
	utils.AddConfigString(Flag["Delete"], "delete the migration with the provided filename", "", cmd, config)
	utils.AddConfigBool(Flag["Debug"], "run the application in debug mode (show full dump on exception thrown)", false, cmd, config)
	utils.AddConfigBool(Flag["Down"], "perform a downward migration", false, cmd, config)
	utils.AddConfigBool(Flag["Up"], "perform an upward migration", true, cmd, config)
	utils.AddConfigInt(Flag["Steps"], "number of migration steps to perform, a -1 value runs all upwards migrations (--up/nothing specified), but only 1 downward migration (--down flag specified)", -1, cmd, config)
	utils.AddConfigString(Flag["Driver"], "database driver", "driver", cmd, config)
	utils.AddConfigString(Flag["Username"], "database user", "username", cmd, config)
	utils.AddConfigString(Flag["Password"], "password for database user [username]", "password", cmd, config)
	utils.AddConfigString(Flag["Host"], "hostname/address which database service can be reached from", "127.0.0.1", cmd, config)
	utils.AddConfigString(Flag["Port"], "port which database service is listening on", "3306", cmd, config)
	utils.AddConfigString(Flag["Database"], "database (schema) name to connect to by default", "database", cmd, config)
	utils.AddConfigBool(Flag["Verbose"], "show verbose logging", false, cmd, config)
	utils.AddConfigBool(Flag["Yes"], "automatically apply migration operations without asking", false, cmd, config)
	config.AutomaticEnv()
	return cmd
}

// getArgsProcessor returns a function that cobra can use for its Args
// property that defines a function that validates the arguments, the
// long logic below is for retrieving the values from the environment
// variables instead of from the flags. see https://github.com/spf13/viper/issues/671
// for more information on why this is needed.
func getArgsProcessor(config *viper.Viper) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("the path to the migrations should be specified")
		}
		cmd.Flags().VisitAll(func(f *pflag.Flag) {
			switch f.Value.Type() {
			case "string":
				configValue := config.GetString(f.Name)
				if len(configValue) != 0 {
					cmd.Flag(f.Name).Value.Set(configValue)
				}
			case "bool":
				configValue := config.GetBool(f.Name)
				if !configValue {
					cmd.Flag(f.Name).Value.Set("false")
				} else {
					cmd.Flag(f.Name).Value.Set("true")
				}
			}
		})
		return nil
	}
}
