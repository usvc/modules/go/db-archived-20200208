package main

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	migrateCommand "gitlab.com/usvc/modules/go/db/cmd/godb-migrate/command"
)

var command *cobra.Command
var config *viper.Viper

func init() {
	config = viper.New()
	command = &cobra.Command{
		Use:     "godb <subcommand>",
		Short:   "godb is a database migration cli tool",
		Long:    `An all-purpose tool for administering databases`,
		Example: "",
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("hello")
		},
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
	}
	// > add new commands below
	command.AddCommand(migrateCommand.Get(config))
	// / add new commands above
}

func main() {
	command.Execute()
}
