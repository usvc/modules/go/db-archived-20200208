package command

import (
	"errors"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	. "gitlab.com/usvc/modules/go/db/lib/config"
	"gitlab.com/usvc/modules/go/db/lib/utils"
)

const (
	Usage            = "check [flags] "
	DescriptionShort = "check is a database checker cli"
	DescriptionLong  = "A database migrator tool for Go applications"
	Example          = "" +
		"  godb-check -dmysql -uuser -ppassword -Hlocalhost -P3306 processes\n" +
		"  godb-check -dpostgres -uuser -ppassword -Hlocalhost -P5432 processes"
)

// Run defines the logical flow of the command
func Run(cmd *cobra.Command, args []string) {
	DebugMode = cmd.Flag(Flag["Debug"][FlagLong]).Value.String() == "true"
	defer utils.HandleExceptionsGracefully(DebugMode, func() { os.Exit(127) })
	Driver = cmd.Flag(Flag["Driver"][FlagLong]).Value.String()
	connection := utils.CreateConnection(cmd)

	typeOfCheck := args[0]
	switch typeOfCheck {
	case "connections":
		fallthrough
	case "connection":
		HandleConnections(cmd, connection)
	case "tables":
		fallthrough
	case "table":
		HandleTables(cmd, connection)
	case "ping":
		HandlePing(cmd, connection)
	case "processes":
		HandleProcesses(cmd, connection)
	}

	os.Exit(0)
}

// Get is used by the calling command to retrieve this for use
// as a sub-command
func Get(config *viper.Viper) *cobra.Command {
	cmd := &cobra.Command{
		Use:     Usage,
		Short:   DescriptionShort,
		Long:    DescriptionLong,
		Example: Example,
		Run:     Run,
		Args:    getArgsProcessor(config),
	}
	utils.AddConfigBool(Flag["Debug"], "run the application in debug mode (show full dump on exception thrown)", false, cmd, config)
	utils.AddConfigString(Flag["Driver"], "database driver", "driver", cmd, config)
	utils.AddConfigString(Flag["Username"], "database user", "username", cmd, config)
	utils.AddConfigString(Flag["Password"], "password for database user [username]", "password", cmd, config)
	utils.AddConfigString(Flag["Host"], "hostname/address which database service can be reached from", "127.0.0.1", cmd, config)
	utils.AddConfigString(Flag["Port"], "port which database service is listening on", "3306", cmd, config)
	utils.AddConfigString(Flag["Database"], "database (schema) name to connect to by default", "database", cmd, config)
	utils.AddConfigString(Flag["Output"], "defines the output type for the check", "text", cmd, config)
	config.AutomaticEnv()
	return cmd
}

// getArgsProcessor returns a function that cobra can use for its Args
// property that defines a function that validates the arguments, the
// long logic below is for retrieving the values from the environment
// variables instead of from the flags. see https://github.com/spf13/viper/issues/671
// for more information on why this is needed.
func getArgsProcessor(config *viper.Viper) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("the path to the migrations should be specified")
		}
		cmd.Flags().VisitAll(func(f *pflag.Flag) {
			switch f.Value.Type() {
			case "string":
				configValue := config.GetString(f.Name)
				if len(configValue) != 0 {
					cmd.Flag(f.Name).Value.Set(configValue)
				}
			case "bool":
				configValue := config.GetBool(f.Name)
				if !configValue {
					cmd.Flag(f.Name).Value.Set("false")
				} else {
					cmd.Flag(f.Name).Value.Set("true")
				}
			}
		})
		return nil
	}
}
