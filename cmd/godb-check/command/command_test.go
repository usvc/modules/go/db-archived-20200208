package command

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/db/lib/interfaces"
)

type CommandTests struct {
	suite.Suite
}

func TestCommand(t *testing.T) {
	suite.Run(t, &CommandTests{})
}

func (s *CommandTests) TestInterface() {
	var _ interfaces.SubcommandGetter = Get
	var _ interfaces.SubcommandRunner = Run
}
