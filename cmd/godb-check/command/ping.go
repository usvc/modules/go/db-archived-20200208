package command

import (
	"database/sql"
	"errors"
	"os"

	. "gitlab.com/usvc/modules/go/db/lib/config"
	. "gitlab.com/usvc/modules/go/db/lib/log"

	"github.com/spf13/cobra"
)

var ErrorTodo = errors.New("TODO: not supported yet")

func HandleConnections(cmd *cobra.Command, connection *sql.DB) {
	switch Driver {
	case "postgres":
		panic(ErrorTodo)
	case "mysql":
		fallthrough
	default:
		row := connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Connections'")
		var label, value string
		row.Scan(&label, &value)
		Log("total connections made so far: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Max_used_connections'")
		row.Scan(&label, &value)
		Log("maximum simultaneous connections: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_empty_query'")
		row.Scan(&label, &value)
		Log("EMPTY queries: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_delete'")
		row.Scan(&label, &value)
		Log("DELETE queries: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_select'")
		row.Scan(&label, &value)
		Log("SELECT queries: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_update'")
		row.Scan(&label, &value)
		Log("UPDATE queries: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_alter_db'")
		row.Scan(&label, &value)
		Log("ALTER DB queries: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_create_db'")
		row.Scan(&label, &value)
		Log("CREATE DB queries: %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_drop_db'")
		row.Scan(&label, &value)
		Log("DROP DB queries: %s", value)
	}
}
func HandleTables(cmd *cobra.Command, connection *sql.DB) {
	switch Driver {
	case "postgres":
		panic(ErrorTodo)
	case "mysql":
		fallthrough
	default:
		Log("[commands]")
		var label, value string
		row := connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_alter_table'")
		row.Scan(&label, &value)
		Log("ALTER         : %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_create_table'")
		row.Scan(&label, &value)
		Log("CREATE        : %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_drop_table'")
		row.Scan(&label, &value)
		Log("DROP          : %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_lock_table'")
		row.Scan(&label, &value)
		Log("LOCK          : %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_rename_table'")
		row.Scan(&label, &value)
		Log("RENAME        : %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_show_create_table'")
		row.Scan(&label, &value)
		Log("SHOW CREATE   : %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_show_table_status'")
		row.Scan(&label, &value)
		Log("SHOW STATUS   : %s", value)
		row = connection.QueryRow("SHOW GLOBAL STATUS LIKE 'Com_show_tables'")
		row.Scan(&label, &value)
		Log("SHOW          : %s", value)
	}
}

func HandlePing(cmd *cobra.Command, connection *sql.DB) {
	switch Driver {
	case "postgres":
		panic(ErrorTodo)
	case "mysql":
		fallthrough
	default:
		row := connection.QueryRow("SELECT 1")
		var value string
		err := row.Scan(&value)
		outputFormat := cmd.Flag(Flag["Output"][FlagLong]).Value.String()
		if err != nil {
			LogKeyValue(LogFormat(outputFormat), "status", err.Error())
			os.Exit(1)
		} else if value == "1" {
			LogKeyValue(LogFormat(outputFormat), "status", "ok")
			os.Exit(0)
		} else {
			LogKeyValue(LogFormat(outputFormat), "status", "not ok")
			os.Exit(1)
		}
	}
}
