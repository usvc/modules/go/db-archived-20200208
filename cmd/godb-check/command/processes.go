package command

import (
	"database/sql"

	. "gitlab.com/usvc/modules/go/db/lib/config"
	. "gitlab.com/usvc/modules/go/db/lib/log"

	"github.com/spf13/cobra"
)

const (
	ProcessQueryMySQL = "SELECT user, db, command, time, state, info FROM information_schema.processlist"
)

type ProcessesStatistics struct {
	TotalConnections float64 `json:"total_connections"`
	TotalTime        float64 `json:"total_time"`
	AverageTime      float64 `json:"average_time"`
	DatabaseCount    uint    `json:"database_count"`
	StateCount       uint    `json:"state_count"`
	UserCount        uint    `json:"user_count"`
	QueryCount       uint    `json:"query_count"`
}

func (stats *ProcessesStatistics) GetAverageTime() float64 {
	return stats.TotalTime / stats.TotalConnections
}

type Process struct {
	User     string `json:"user"`
	Database string `json:"database"`
	Command  string `json:"command"`
	Time     int    `json:"time"`
	State    string `json:"state"`
	Info     string `json:"info"`
}

type ProcessesCheck struct {
	Statistics ProcessesStatistics `json:"statistics"`
	Database   map[string]uint     `json:"database"`
	State      map[string]uint     `json:"state"`
	User       map[string]uint     `json:"user"`
	Query      map[string]uint     `json:"query"`
	Processes  []Process
}

func (processesCheck *ProcessesCheck) LoadStatistics() {
	for range processesCheck.Database {
		processesCheck.Statistics.DatabaseCount++
	}
	for range processesCheck.User {
		processesCheck.Statistics.UserCount++
	}
	for range processesCheck.State {
		processesCheck.Statistics.StateCount++
	}
	for range processesCheck.Query {
		processesCheck.Statistics.QueryCount++
	}
	processesCheck.Statistics.AverageTime = processesCheck.Statistics.GetAverageTime()
}

func NewProcessesCheck() ProcessesCheck {
	return ProcessesCheck{
		Statistics: ProcessesStatistics{},
		Database:   map[string]uint{},
		State:      map[string]uint{},
		User:       map[string]uint{},
		Query:      map[string]uint{},
		Processes:  []Process{},
	}
}

func HandleProcesses(cmd *cobra.Command, connection *sql.DB) {
	outputFormat := LogFormat(cmd.Flag(Flag["Output"][FlagLong]).Value.String())
	processesCheck := NewProcessesCheck()
	switch Driver {
	case "postgres":
		panic("TODO: not supported yet")
	case "mysql":
		fallthrough
	default:
		rows, err := connection.Query(ProcessQueryMySQL)
		if err != nil {
			panic(err)
		}
		for rows.Next() {
			var (
				time                  int
				command               string
				user, db, info, state sql.NullString
			)
			rows.Scan(&user, &db, &command, &time, &state, &info)
			processesCheck.Processes = append(processesCheck.Processes, Process{
				User:     user.String,
				Time:     time,
				Command:  command,
				Database: db.String,
				Info:     info.String,
				State:    state.String,
			})
			processesCheck.Statistics.TotalTime += float64(time)
			processesCheck.Statistics.TotalConnections += 1
			if db.Valid && len(db.String) > 0 {
				processesCheck.Database[db.String]++
			} else {
				processesCheck.Database["null"]++
			}
			if user.Valid && len(user.String) > 0 {
				processesCheck.User[user.String]++
			} else {
				processesCheck.User["null"]++
			}
			if info.Valid && len(info.String) > 0 {
				processesCheck.Query[info.String]++
			} else {
				processesCheck.Query["null"]++
			}
			if state.Valid && len(state.String) > 0 {
				processesCheck.State[state.String]++
			} else {
				processesCheck.State["null"]++
			}
		}
		processesCheck.LoadStatistics()
		LogKeyValue(outputFormat, "data", processesCheck)
	}
}
