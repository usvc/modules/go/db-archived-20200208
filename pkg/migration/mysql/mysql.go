package mysql

import (
	hasher "crypto/sha1"
	"fmt"
	"path"

	"gitlab.com/usvc/modules/go/db/pkg/migration/utils"
)

// New takes in the path to a file that should be a migration
// and parses it into a Migration data structure
func New(pathToMigration string) (*Migration, error) {
	filename := path.Base(pathToMigration)
	datestamp, err := utils.GetDatestampFromFilename(filename)
	if err != nil {
		return nil, err
	}
	description := utils.GetDescriptionFromFilename(filename)
	content, err := utils.GetFileContent(pathToMigration)
	if err != nil {
		return nil, err
	}
	contentHash := fmt.Sprintf("%x", hasher.Sum(content))
	return &Migration{
		Content:     string(content),
		ContentHash: contentHash,
		Datestamp:   datestamp,
		Description: description,
		Filename:    filename,
	}, nil
}
