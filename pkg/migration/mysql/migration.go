package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/usvc/modules/go/db/pkg/constants"
)

// Migration represents the structure of a migration that can be
// stored in a database
type Migration struct {
	// ID is a unique identifier for the migration
	ID int64 `mysql:"id INT PRIMARY KEY AUTO_INCREMENT"`
	// Filename contains the migration filename, eg. 20191104_100000_some_descriptive_action.sql
	Filename string `mysql:"filename VARCHAR(256) UNIQUE NOT NULL"`
	// Datestamp contains the datestamp of the migration, eg. 20191104_100000
	Datestamp time.Time `mysql:"datestamp VARCHAR(16) NOT NULL"`
	// Description contains the string section of the migration, eg. some_descriptive_action
	Description string `mysql:"description VARCHAR(240)"`
	// ContentHash contains an MD5 hash of the file content
	ContentHash string `mysql:"content_hash VARCHAR(64) NOT NULL"`
	// Content contains the exact content of the migration
	Content string `mysql:"content TEXT NOT NULL"`
	// Applied denotes whether the migration has been applied
	Applied bool `mysql:"applied TINYINT(1) DEFAULT 0"`
	// AppliedAt indicates when the migration was applied
	AppliedAt time.Time `mysql:"applied_at TIMESTAMP DEFAULT NOW()"`
}

// InsertToDB inserts the migration into the migration table with
// the `applied` field unset, also sets the ID property on success
func (m *Migration) InsertToDB(connection *sql.DB) (int64, error) {
	unsetTime := time.Time{}
	switch true {
	case len(m.GetFilename()) == 0:
		return constants.ErrorInt64, errors.New("unable to begin a migration when the Filename is not set")
	case len(m.GetContent()) == 0:
		return constants.ErrorInt64, errors.New("unable to begin a migration when the Content is not set")
	case len(m.GetContentHash()) == 0:
		return constants.ErrorInt64, errors.New("unable to begin a migration when the ContentHash is not set")
	case m.GetDatestamp() == unsetTime:
		return constants.ErrorInt64, errors.New("unable to begin a migration when the Datestamp is not set")
	}
	insertToDBStatement, err := connection.Prepare(insertToDBStatement)
	if err != nil {
		return constants.ErrorInt64, err
	}
	res, err := insertToDBStatement.Exec(
		m.GetFilename(),
		m.GetFormattedDatestamp(),
		m.GetDescription(),
		m.GetContentHash(),
		m.GetContent(),
	)
	if err != nil {
		return constants.ErrorInt64, err
	}
	insertedID, err := res.LastInsertId()
	m.ID = insertedID
	return m.ID, err
}

// MarkAppliedInDB completes the migration by changing it's `applied` flag
// to be truthy
func (m *Migration) MarkAppliedInDB(connection *sql.DB) error {
	if m.GetID() == 0 {
		return errors.New("this migration instance doesn't seem to exist in the remote database")
	}
	MarkAppliedInDBStatement, err := connection.Prepare(markAppliedInDBStatement)
	if err != nil {
		return err
	}
	if _, err = MarkAppliedInDBStatement.Exec(m.GetID()); err != nil {
		return err
	}
	return nil
}

func (m *Migration) RemoveFromDB(connection *sql.DB) error {
	var unsetID int64
	if m.GetID() == unsetID {
		return errors.New("the ID property should be set to confirm it exists in the migration table")
	}
	removeStatement, err := connection.Prepare(removeFromDBStatement)
	if err != nil {
		return err
	}
	res, err := removeStatement.Exec(m.GetID())
	if err != nil {
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	} else if rowsAffected == 0 {
		return errors.New("no rows were removed")
	}
	m.Applied = false
	m.ID = 0
	return nil
}

func (m *Migration) LoadFromDB(connection *sql.DB) error {
	var loadStatement *sql.Stmt
	var migrationRow *sql.Row
	var err error
	if err = connection.Ping(); err != nil {
		return err
	}
	loadStatement, err = connection.Prepare(loadFromDBStatement)
	if err != nil {
		return err
	}
	var loadedID int64
	var loadedDatestamp string
	var loadedContent, loadedContentHash string
	var loadedApplied bool
	migrationRow = loadStatement.QueryRow(m.Filename)
	if err = migrationRow.Scan(&loadedID, &loadedDatestamp, &loadedContent, &loadedContentHash, &loadedApplied); err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		return err
	}
	if len(m.Content) > 0 && m.Content != loadedContent {
		return fmt.Errorf(
			"content of locally available migration (%s) seems different from the remote migration (respectively):\n'''sql\n%s\n'''\n\n---VS---\n\n'''sql\n%s\n'''",
			m.Filename,
			m.Content,
			loadedContent,
		)
	}
	if len(m.ContentHash) > 0 && m.ContentHash != loadedContentHash {
		return fmt.Errorf(
			"content hash of locally available migration (%s) seems different from the remote migration (respectively):\n'''sql\n%s\n'''\n\n---VS---\n\n'''sql\n%s\n'''",
			m.Filename,
			m.ContentHash,
			loadedContentHash,
		)
	}
	m.ID = loadedID
	m.Applied = loadedApplied
	return err
}

func (m Migration) Validate() error {
	migrationDirections := strings.Split(m.Content, constants.MigrationSeparator)
	if len(migrationDirections) < 2 {
		return fmt.Errorf("a forward and backward migration could not be found in the content: '%s'", m.Content)
	} else if len(migrationDirections) > 2 {
		return fmt.Errorf("more than just a forward and backward migration was found in the content: '%s'", m.Content)
	}
	return nil
}

// Execute performs the database migration using data stored in the
// current Migration instance using the provided connection argument
func (m Migration) Execute(connection *sql.DB) error {
	if err := connection.Ping(); err != nil {
		return err
	}
	migrationDirections := strings.Split(m.Content, constants.MigrationSeparator)
	if len(migrationDirections) == 2 {
		transactionContext, cancelTransaction := context.WithTimeout(context.Background(), constants.TransactionTimeout)
		defer cancelTransaction()
		_, err := connection.ExecContext(transactionContext, migrationDirections[constants.ForwardMigration])
		return err
	}
	return fmt.Errorf("a forward and backward migration could not be found in the content: '%s'", m.Content)
}

// Rollback performs the database migration using data stored in the
// current Migration instance using the provided connection argument
func (m Migration) Rollback(connection *sql.DB) error {
	if err := connection.Ping(); err != nil {
		return err
	}
	migrationDirections := strings.Split(m.Content, constants.MigrationSeparator)
	if len(migrationDirections) == 2 {
		transactionContext, cancelTransaction := context.WithTimeout(context.Background(), constants.TransactionTimeout)
		defer cancelTransaction()
		_, err := connection.ExecContext(transactionContext, migrationDirections[constants.BackwardMigration])
		return err
	}
	return fmt.Errorf("a forward and backward migration could not be found in the content: '%s'", m.Content)
}

// Getapplied retrieves the migration's applied state
func (m Migration) GetApplied() bool {
	return m.Applied
}

// GetContent retrieves the migration content
func (m Migration) GetContent() string {
	return m.Content
}

// GetContentHash retrieves the hash of the content
func (m Migration) GetContentHash() string {
	return m.ContentHash
}

// GetDatestamp retrieves the indicated datetime of the migration
func (m Migration) GetDatestamp() time.Time {
	return m.Datestamp
}

// GetDescription retrieves the description of the migration
func (m Migration) GetDescription() string {
	return m.Description
}

// GetFilename retrieves the filename of the migration
func (m Migration) GetFilename() string {
	return m.Filename
}

// GetFormattedDatestamp retrieves the indicated datetime of the migration as a string
func (m Migration) GetFormattedDatestamp() string {
	return m.GetDatestamp().Format(constants.DatestampFormat)
}

// GetID retrieves the ID of the migration
func (m Migration) GetID() int64 {
	return m.ID
}
