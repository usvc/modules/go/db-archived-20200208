package mysql

import (
	"fmt"

	"gitlab.com/usvc/modules/go/db/pkg/constants"
)

var (
	insertToDBStatement = fmt.Sprintf(
		"INSERT INTO %s (filename, datestamp, description, content_hash, content) VALUES (?, ?, ?, ?, ?)",
		constants.MigrationTableName,
	)
	loadFromDBStatement = fmt.Sprintf(
		"SELECT id, datestamp, content, content_hash, applied FROM %s WHERE filename = ?",
		constants.MigrationTableName,
	)
	markAppliedInDBStatement = fmt.Sprintf(
		"UPDATE %s SET applied = 1 WHERE id = ?",
		constants.MigrationTableName,
	)
	removeFromDBStatement = fmt.Sprintf(
		"DELETE FROM %s WHERE id = ?",
		constants.MigrationTableName,
	)
)
