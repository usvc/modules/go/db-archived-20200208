package mysql

import (
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/db/pkg/mysql"
)

type MigrationTests struct {
	suite.Suite
}

func TestMigration(t *testing.T) {
	suite.Run(t, &MigrationTests{})
}

func (s *MigrationTests) TestExecute() {
	conn, err := mysql.CreateConnection(mysql.CreateDSN(
		"user",
		"password",
		"127.0.0.1",
		"13306",
		"database",
		nil,
	))
	s.NotNil(conn, "an instance of an sql connection was expected")
	s.Nil(err, "no errors expected while connecting to the database")
	cwd, err := os.Getwd()
	s.Nil(err, "no errors expected while retrieving working directory")
	migrationFilePath := path.Join(cwd, "../../../tests/suites/non-specific/suite_00/20111213_141516_do_something.sql")
	migration, err := New(migrationFilePath)
	s.Nil(err)
	err = migration.Execute(conn)
	s.Nil(err, "no errors expected when running the test migration")
	err = migration.Rollback(conn)
	s.Nil(err, "no errors expected when rolling back the test migration")
}
