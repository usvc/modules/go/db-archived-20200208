package mysql

import (
	"crypto/sha1"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/suite"
)

type MySQLTests struct {
	filename string
	suite.Suite
}

func TestMySQL(t *testing.T) {
	suite.Run(t, &MySQLTests{})
}

func (s *MySQLTests) SetupTest() {
	s.filename = "20191109_233300_some_description.sql"
}

func (s *MigrationTests) TestNew() {
	cwd, err := os.Getwd()
	s.Nil(err, "no error expected while retrieving the current directory")
	migrationFilePath := path.Join(cwd, "../../../tests/suites/non-specific/suite_00/20111213_141516_do_something.sql")
	migration, err := New(migrationFilePath)
	s.Nil(err)
	s.Equal(path.Base(migrationFilePath), migration.GetFilename(),
		"filename is initialized correctly")
	s.Equal(2011, migration.GetDatestamp().Year(),
		"year in the datestamp is initialized correctly")
	s.Equal(12, int(migration.GetDatestamp().Month()),
		"month in the datestamp is initialized correctly")
	s.Equal(13, migration.GetDatestamp().Day(),
		"day of the month in the datestamp is initialized correctly")
	s.Equal(14, migration.GetDatestamp().Hour(),
		"hour in the datestamp is initialized correctly")
	s.Equal(15, migration.GetDatestamp().Minute(),
		"minute in the datestamp is initialized correctly")
	s.Equal(16, migration.GetDatestamp().Second(),
		"seconds in the datestamp is initialized correctly")
	s.Equal("do_something", migration.GetDescription(),
		"description is initialized correctly")
	content, err := ioutil.ReadFile(migrationFilePath)
	s.Nil(err, "No errors was expected while reading the migration file")
	s.Equal(string(content), migration.GetContent(),
		"file content is loaded correctly")
	hash := fmt.Sprintf("%x", sha1.Sum(content))
	s.Equal(hash, migration.GetContentHash(),
		"content hash is loaded correctly")
}
