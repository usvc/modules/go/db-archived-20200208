package mysql

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/db/pkg/constants"
)

type SQLCommandsTest struct {
	suite.Suite
}

func TestSQLCommands(t *testing.T) {
	suite.Run(t, &SQLCommandsTest{})
}

func (s *SQLCommandsTest) Test_insertToDBStatement() {
	s.Contains(insertToDBStatement, fmt.Sprintf("INSERT INTO %s", constants.MigrationTableName))
}

func (s *SQLCommandsTest) Test_loadFromDBStatement() {
	s.Contains(loadFromDBStatement, fmt.Sprintf("FROM %s WHERE", constants.MigrationTableName))
}

func (s *SQLCommandsTest) Test_markAppliedInDBStatement() {
	s.Contains(markAppliedInDBStatement, fmt.Sprintf("UPDATE %s", constants.MigrationTableName))
}

func (s *SQLCommandsTest) Test_removeFromDBStatement() {
	s.Contains(removeFromDBStatement, fmt.Sprintf("DELETE FROM %s", constants.MigrationTableName))
}
