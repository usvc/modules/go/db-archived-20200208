package migration

import (
	"database/sql"
	"time"
)

type Migration interface {
	// InsertToDB should start the migration by inserting a
	// row into the migrations table with the `applied`
	// flag set to false
	InsertToDB(connection *sql.DB) (int64, error)
	// RemoveFromDB should remove the row in the migrations table
	// corresponding to the migration ID
	RemoveFromDB(connection *sql.DB) error
	// MarkAppliedInDB sets the migration information in the database
	// to be marked as applied
	MarkAppliedInDB(connection *sql.DB) error
	// LoadFromDB loads the migration data available in the database into
	// the current instance of a Migration
	LoadFromDB(connection *sql.DB) error
	// Execute runs a migration
	Execute(connection *sql.DB) error
	// Rollback rolls back a migration
	Rollback(connection *sql.DB) error
	// Validate makes sure the migration is correctly formatted
	Validate() error
	// GetApplied retrieves the applied status of the migration
	GetApplied() bool
	// GetContent retrieves the file contents of the migration
	GetContent() string
	// GetContentHash retrieves the hash of the file contents of the migration
	// (use to check if the content is the same)
	GetContentHash() string
	// GetDatestamp retrieves the datestamp of the migration
	GetDatestamp() time.Time
	// GetDescription retrieves the description of the migration
	GetDescription() string
	// GetFilename retrieves the filename of the migration
	GetFilename() string
	// GetFormattedDatestamp retrieves the datestamo if the migration as a string
	GetFormattedDatestamp() string
	// GetID retrieves the ID of the migration
	GetID() int64
}
