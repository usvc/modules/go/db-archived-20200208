package utils

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"gitlab.com/usvc/modules/go/db/pkg/constants"
)

type Doer interface {
	Do(*sql.DB) error
}

func GetDatestampFromFilename(filename string) (time.Time, error) {
	unsetTime := time.Time{}
	if len(filename) < len(constants.DatestampFormat) {
		return unsetTime, fmt.Errorf("the file with filename '%s' does not seem to contain a datestamp", filename)
	}
	datestamp, err := time.Parse(constants.DatestampFormat, filename[:len(constants.DatestampFormat)])
	if err != nil {
		return unsetTime, err
	}
	return datestamp, nil
}

func GetDescriptionFromFilename(filename string) string {
	return strings.Trim(filename[len(constants.DatestampFormat):strings.LastIndex(filename, ".")], "-_")
}

func GetFileContent(pathToFile string) ([]byte, error) {
	content, err := ioutil.ReadFile(pathToFile)
	if err != nil {
		return nil, err
	}
	return content, nil
}
