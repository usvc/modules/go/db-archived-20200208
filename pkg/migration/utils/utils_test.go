package utils

import (
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/db/pkg/constants"
)

type UtilsTests struct {
	filename string
	suite.Suite
}

func TestUtils(t *testing.T) {
	suite.Run(t, &UtilsTests{})
}

func (s *UtilsTests) SetupTest() {
	s.filename = "20191109_233300_some_description.sql"
}

func (s *UtilsTests) TestGetDatestampFromFilename() {
	defer func() {
		r := recover()
		s.Nil(r, "no error should have been thrown given a valid filename")
	}()
	datestamp, err := GetDatestampFromFilename(s.filename)
	s.Nil(err)
	datestampAsString := datestamp.Format(constants.DatestampFormat)
	s.Equal("20191109_233300", datestampAsString)
}

func (s *UtilsTests) TestGetDescriptionFromFilename() {
	description := GetDescriptionFromFilename(s.filename)
	s.Equal("some_description", description)
}

func (s *UtilsTests) TestGetFileContent() {
	defer func() {
		r := recover()
		s.Nil(r)
	}()
	cwd, err := os.Getwd()
	s.Nil(err, "no error was expected while getting the current directory")
	fileContent, err := GetFileContent(path.Join(cwd, "../../../tests/files/content_test"))
	s.Nil(err)
	s.Equal("content_test", string(fileContent))
}
