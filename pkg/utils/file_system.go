package utils

import (
	"io/ioutil"
	"path"
)

func GetDirectoryListing(pathToDirectory string) ([]string, error) {
	var listing []string
	files, err := ioutil.ReadDir(pathToDirectory)
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(files); i++ {
		if !files[i].IsDir() {
			listing = append(listing, path.Join(pathToDirectory, files[i].Name()))
		}
	}
	return listing, nil
}
