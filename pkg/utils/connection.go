package utils

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

type ConnectionCreator func(string) (*sql.DB, error)

func CreateConnection(driver, dsn string) (*sql.DB, error) {
	connection, err := sql.Open(driver, dsn)
	if err != nil {
		return nil, err
	}
	err = connection.Ping()
	if err != nil {
		return nil, err
	}
	return connection, nil
}
