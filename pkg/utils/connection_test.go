package utils

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type UtilsTests struct {
	suite.Suite
}

func TestUtils(t *testing.T) {
	suite.Run(t, &UtilsTests{})
}

func (s *UtilsTests) TestCreateConnection() {
	connection, err := CreateConnection("mysql", "user:password@(127.0.0.1:13306)/database")
	s.NotNil(connection)
	s.Nil(err)
}
