package utils

import (
	"fmt"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/suite"
)

type FileSystemTests struct {
	suite.Suite
}

func TestFilesystem(t *testing.T) {
	suite.Run(t, &FileSystemTests{})
}

func (s *FileSystemTests) TestGetDirectoryListing() {
	cwd, err := os.Getwd()
	s.Nil(err)
	testDirPath := path.Join(cwd, "../../tests/suites/non-specific/file-listing")
	listings, err := GetDirectoryListing(testDirPath)
	s.Nil(err)
	for i := 0; i < len(listings); i++ {
		s.Equal(fmt.Sprintf("%v", i+1), path.Base(listings[i]))
	}
}
