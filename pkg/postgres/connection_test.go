package postgres

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

const (
	Postgres9DSN  = "postgres://user:password@127.0.0.1:15432/database?sslmode=disable"
	Postgres10DSN = "postgres://user:password@127.0.0.1:15433/database?sslmode=disable"
	Postgres11DSN = "postgres://user:password@127.0.0.1:15434/database?sslmode=disable"
	Postgres12DSN = "postgres://user:password@127.0.0.1:15435/database?sslmode=disable"
)

type ConnectionTests struct {
	suite.Suite
}

func TestConnection(t *testing.T) {
	suite.Run(t, &ConnectionTests{})
}

func (s *ConnectionTests) TestCreateConnection_Postgres_9() {
	connection, err := CreateConnection(Postgres9DSN)
	s.NotNil(connection)
	s.Nil(err)
}

func (s *ConnectionTests) TestCreateConnection_Postgres_10() {
	connection, err := CreateConnection(Postgres10DSN)
	s.NotNil(connection)
	s.Nil(err)
}

func (s *ConnectionTests) TestCreateConnection_Postgres_11() {
	connection, err := CreateConnection(Postgres11DSN)
	s.NotNil(connection)
	s.Nil(err)
}

func (s *ConnectionTests) TestCreateConnection_Postgres_12() {
	connection, err := CreateConnection(Postgres12DSN)
	s.NotNil(connection)
	s.Nil(err)
}
