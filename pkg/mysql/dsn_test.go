package mysql

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
)

type DSNTests struct {
	suite.Suite
}

func TestDSN(t *testing.T) {
	suite.Run(t, &DSNTests{})
}

func (s *DSNTests) TestCreateDSN_withParameters() {
	dsn := CreateDSN(
		"user",
		"password",
		"hostname",
		"port",
		"database",
		map[string]string{
			"param1": "1",
			"param2": "two",
			"param3": "with spaces",
			"param4": "with-dashes",
			"param5": "with_underscores",
		},
	)
	dsnSections := strings.Split(dsn, "?")
	s.Equal("user:password@(hostname:port)/database", dsnSections[0])
	params := strings.Split(dsnSections[1], "&")
	s.Contains(params, "param1=1")
	s.Contains(params, "param2=two")
	s.Contains(params, "param3=with+spaces")
	s.Contains(params, "param4=with-dashes")
	s.Contains(params, "param5=with_underscores")
}

func (s *DSNTests) TestCreateDSN_withoutParameters() {
	dsn := CreateDSN(
		"user",
		"password",
		"hostname",
		"port",
		"database",
	)
	s.Equal("user:password@(hostname:port)/database", dsn)
}
