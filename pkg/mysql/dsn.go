package mysql

import (
	"fmt"
	"net/url"
	"strings"
)

func CreateDSN(
	user,
	password,
	hostname,
	port,
	database string,
	parameters ...map[string]string,
) string {
	parameterString := "?"
	if len(parameters) > 0 && parameters[0] != nil {
		for key, value := range parameters[0] {
			parameterString += fmt.Sprintf(
				"%s=%s&",
				url.QueryEscape(key),
				url.QueryEscape(value),
			)
		}
	}
	parameterString = strings.TrimRight(strings.Trim(parameterString, "&"), "?")
	return fmt.Sprintf(
		"%s:%s@(%s:%s)/%s%s",
		user,
		password,
		hostname,
		port,
		database,
		parameterString,
	)
}
