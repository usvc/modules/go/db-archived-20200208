package mysql

import (
	"database/sql"

	"gitlab.com/usvc/modules/go/db/pkg/utils"
)

func CreateConnection(dsn string) (*sql.DB, error) {
	return utils.CreateConnection("mysql", dsn)
}
