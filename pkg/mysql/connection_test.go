package mysql

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

const (
	MySQL5DSN    = "user:password@(127.0.0.1:13306)/database"
	MySQL8DSN    = "user:password@(127.0.0.1:13307)/database"
	MariaDB10DSN = "user:password@(127.0.0.1:13308)/database"
)

type ConnectionTests struct {
	suite.Suite
}

func TestConnection(t *testing.T) {
	suite.Run(t, &ConnectionTests{})
}

func (s *ConnectionTests) TestCreateConnection_MySQL_5() {
	connection, err := CreateConnection(MySQL5DSN)
	s.NotNil(connection)
	s.Nil(err)
}

func (s *ConnectionTests) TestCreateConnection_MySQL_8() {
	connection, err := CreateConnection(MySQL8DSN)
	s.NotNil(connection)
	s.Nil(err)
}

func (s *ConnectionTests) TestCreateConnection_MariaDB_10() {
	connection, err := CreateConnection(MariaDB10DSN)
	s.NotNil(connection)
	s.Nil(err)
}
