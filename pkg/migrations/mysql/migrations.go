package mysql

import (
	"database/sql"

	"gitlab.com/usvc/modules/go/db/pkg/migration"
)

type Migrations []migration.Migration

// Execute runs `steps[0]` number of forward migrations
func (m Migrations) Execute(connection *sql.DB, steps ...int64) {
	var err error
	processed := int64(0)
	for i := 0; i < m.Len(); i++ {
		if err = m[i].LoadFromDB(connection); err != nil {
			panic(err)
		}
		exists := m[i].GetID() != 0
		applied := m[i].GetApplied()
		if validationErrors := m[i].Validate(); validationErrors != nil {
			panic(validationErrors)
		}
		if !applied {
			if !exists {
				if _, err = m[i].InsertToDB(connection); err != nil {
					panic(err)
				}
			}
			if err = m[i].Execute(connection); err != nil {
				panic(err)
			} else if err = m[i].MarkAppliedInDB(connection); err != nil {
				panic(err)
			}
			processed++
			if len(steps) > 0 && steps[0] >= 0 && processed >= steps[0] {
				return
			}
		}
	}
}

// ForEach runs through each migration calling the handler function
// for each item in the stored slice
func (m Migrations) ForEach(handler func(migration.Migration, int)) {
	for i := 0; i < m.Len(); i++ {
		handler(m[i], i)
	}
}

// Len implements sort.Interface and returns the length of this
// slice of Migrations
func (m Migrations) Len() int {
	return len(m)
}

// Less implements sort.Interface, returns true if Migration at
// index `i` comes before Migration at index `j`
func (m Migrations) Less(i, j int) bool {
	return m[i].GetDatestamp().Before(m[j].GetDatestamp())
}

// Push adds a migration at the end of the Migration slice
func (m *Migrations) Push(someMigration migration.Migration) {
	*m = append(*m, someMigration)
}

// Rollback rolls back `steps[0]` number of backward migrations
func (m Migrations) Rollback(connection *sql.DB, steps ...int64) {
	var err error
	processed := int64(0)
	for i := m.Len() - 1; i >= 0; i-- {
		if err := m[i].LoadFromDB(connection); err != nil {
			panic(err)
		}
		exists := m[i].GetID() != 0
		applied := m[i].GetApplied()
		if exists {
			if applied {
				if err = m[i].Rollback(connection); err != nil {
					panic(err)
				}
			}
			if err = m[i].RemoveFromDB(connection); err != nil {
				panic(err)
			}
			processed++
			if len(steps) > 0 && processed >= steps[0] {
				return
			}
		}
	}
}

// Swap implements sort.Interface and swaps the elements at the
// `i` and `j` 0-indexed positions
func (m Migrations) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}
