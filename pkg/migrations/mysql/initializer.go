package mysql

import (
	"database/sql"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/usvc/modules/go/db/pkg/constants"
	migration "gitlab.com/usvc/modules/go/db/pkg/migration/mysql"
)

// TableExists returns whether the migrations table exists or not
func TableExists(connection *sql.DB) bool {
	row := connection.QueryRow(
		fmt.Sprintf("SHOW TABLES LIKE '%s'", constants.MigrationTableName),
	)
	var table string
	row.Scan(&table)
	return len(table) > 0
}

// Initialize creates the table to store migrations
func Initialize(connection *sql.DB) error {
	migration := migration.Migration{}
	migrationType := reflect.TypeOf(migration)
	fieldsCount := migrationType.NumField()
	var fieldsToCreate []string
	for i := 0; i < fieldsCount; i++ {
		fieldsToCreate = append(fieldsToCreate, migrationType.Field(i).Tag.Get("mysql"))
	}
	sqlQuery := fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (\n\t%s\n) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;", constants.MigrationTableName, strings.Join(fieldsToCreate, ",\n\t"))
	_, err := connection.Exec(sqlQuery)
	if err != nil {
		return err
	}
	return nil
}
