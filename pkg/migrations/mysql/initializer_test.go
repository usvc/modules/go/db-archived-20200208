package mysql

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/db/pkg/constants"
	helper "gitlab.com/usvc/modules/go/db/pkg/mysql"
)

type MySQLTests struct {
	suite.Suite
}

func TestMySQL(t *testing.T) {
	suite.Run(t, &MySQLTests{})
}

func (s *MySQLTests) TestInitialize() {
	defer func() {
		r := recover()
		s.Nil(r, "no errors were expected, have you spun up a local database that's available on port 13306?")
	}()
	conn, err := helper.CreateConnection(helper.CreateDSN(
		"user",
		"password",
		"127.0.0.1",
		"13306",
		"database",
		nil,
	))
	s.Nil(err)
	err = Initialize(conn)
	s.Nil(err)
	_, err = conn.Exec(fmt.Sprintf("DROP TABLE %s", constants.MigrationTableName))
	s.Nil(err)
}
