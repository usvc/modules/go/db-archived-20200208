package mysql

import (
	"fmt"
	"os"
	"path"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/db/pkg/constants"
	migration "gitlab.com/usvc/modules/go/db/pkg/migration/mysql"
	"gitlab.com/usvc/modules/go/db/pkg/mysql"
)

type MigrationsTests struct {
	suite.Suite
}

func TestMigrations(t *testing.T) {
	suite.Run(t, &MigrationsTests{})
}

func (s *MigrationsTests) TestSort() {
	migrations := Migrations{
		&migration.Migration{Datestamp: time.Now().Add(time.Second * 3)},
		&migration.Migration{Datestamp: time.Now().Add(time.Second * 2)},
		&migration.Migration{Datestamp: time.Now().Add(time.Second * -3)},
		&migration.Migration{Datestamp: time.Now().Add(time.Second * 4)},
		&migration.Migration{Datestamp: time.Now().Add(time.Second * -1)},
		&migration.Migration{Datestamp: time.Now()},
		&migration.Migration{Datestamp: time.Now().Add(time.Second * 1)},
	}
	sort.Sort(migrations)
	for i := 0; i < len(migrations)-1; i++ {
		s.True(migrations[i].GetDatestamp().Before(migrations[i+1].GetDatestamp()))
	}
}

func (s *MigrationsTests) TestExecuteRollback() {
	connection, err := mysql.CreateConnection(mysql.CreateDSN(
		"user",
		"password",
		"127.0.0.1",
		"13306",
		"database",
		nil,
	))
	s.Nil(err)
	Initialize(connection)
	var mysqlMigrations Migrations
	cwd, err := os.Getwd()
	s.Nil(err, "no error expected while retrieving current directory")
	m, err := migration.New(path.Join(cwd, "../../../tests/suites/non-specific/suite_00/20111213_141516_do_something.sql"))
	s.Nil(err)
	mysqlMigrations.Push(m)
	mysqlMigrations.Execute(connection)
	row := connection.QueryRow(fmt.Sprintf("SELECT datestamp FROM %s", constants.MigrationTableName))
	var datestamp string
	row.Scan(&datestamp)
	s.Equal("20111213_141516", datestamp)
	mysqlMigrations.Rollback(connection)
	_, err = connection.Exec(fmt.Sprintf("DROP TABLE %s", constants.MigrationTableName))
	s.Nil(err)
}
