package migrations

import (
	"database/sql"

	"gitlab.com/usvc/modules/go/db/pkg/migration"
)

// Migrations is implements helper functions for handling a
// slice of Migrations
type Migrations interface {
	Len() int
	Swap(int, int)
	Less(int, int) bool
	Push(migration.Migration)
	Execute(*sql.DB, ...int64)
	Rollback(*sql.DB, ...int64)
	ForEach(func(migration.Migration, int))
}

type Initializer func(connection *sql.DB)
