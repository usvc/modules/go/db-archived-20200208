package mock

import (
	"database/sql"

	"gitlab.com/usvc/modules/go/db/pkg/migration"
)

var (
	CallCount map[string]int
	Arguments map[string][]interface{}
)

// Reset resets the migrations mock
func Reset(fromFunction ...string) {
	if len(fromFunction) > 0 {
		CallCount[fromFunction[0]] = 0
	} else {
		for key := range CallCount {
			CallCount[key] = 0
			Arguments[key] = []interface{}{}
		}
	}
}

func recordCall(fromFunction string, args ...interface{}) {
	CallCount[fromFunction]++
	Arguments[fromFunction] = append(Arguments[fromFunction], args)
}

type Migrations struct {
	LenValue  int
	LessValue bool
	Value     []migration.Migration
}

var Instance = Migrations{}

func (m Migrations) Execute(connection *sql.DB, steps ...int64) {
	recordCall("Execute", connection, steps)
}

func (m Migrations) ForEach(handler func(singleMigration migration.Migration, index int)) {
	recordCall("ForEach", handler)
	for i := 0; i < len(m.Value); i++ {
		handler(m.Value[i], i)
	}
}

func (m Migrations) Len() int {
	recordCall("Len")
	return m.LenValue
}

func (m Migrations) Less(i, j int) bool {
	recordCall("Less", i, j)
	return m.LessValue
}

func (m *Migrations) Push(singleMigration migration.Migration) {
	recordCall("Push", singleMigration)
}

func (m *Migrations) Rollback(connection *sql.DB, steps ...int64) {
	recordCall("Rollback", connection, steps)
}

func (m Migrations) Swap(i, j int) {
	recordCall("Swap", i, j)
}
