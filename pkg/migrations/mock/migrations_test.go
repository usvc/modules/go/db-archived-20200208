package mock

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/usvc/modules/go/db/pkg/migrations"
)

type MockTests struct {
	suite.Suite
}

func TestMock(t *testing.T) {
	suite.Run(t, &MockTests{})
}

func (s *MockTests) TestMockConforms() {
	var _ migrations.Migrations = &Migrations{}

}
