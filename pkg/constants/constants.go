package constants

import "time"

const (
	DatestampFormat    = "20060102_150405"
	MigrationTableName = "migrations"
	ErrorInt64         = -1
	MigrationSeparator = "---|---"
	ForwardMigration   = 0
	BackwardMigration  = 1
	TransactionTimeout = time.Second * 5
)
